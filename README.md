# Auxilium

## Description

This tool help you to add an arguments parse in your shell script.

This support BASH, KSH, ZSH and DASH shell.

For use this, you must source `/usr/share/auxilium/auxilium.sh` at the beginning of your script and call `auxilium_init` function (with appname, subcommand name and decription arguments).

After this, you can add argument in your main script or other sourced script.

You can print usage of your script.

Add the end of your script you must call the `auxilium_close` function for delete temporaries files

You can use `/usr/bin/auxilium-test` for an script example.

## Usage

* Source auxilium (mandatory): `. /usr/share/auxilium/auxilium.sh`
* Call auxilium_init (mandatory) with this params
  * -s Subcommand name if necessary
  * -m Description of application
  * NAME Mandatory, application Name
* Call auxilium_add for each argument supported by your script:
  * first argument of the function is the name of your script argument
  * -t type of argument (str by default|opt|positional)
  * -s Short option
  * -d Default value
  * -a All remaining arguments, must be last option and no positional argument
  * -l List of choices separate by |
  * -c Callback fonction (Arg in parameter)
  * -m Help Message
* Parse your script arguments by calling auxilium_parse function: `auxilium_parse "$@"`
* You can add arguments in other script file if it is sourcing between auxilium_init and   auxilium_parse functions.
* You can display the usage off your script by calling the auxilium_usage function
* Your arguments value are available in variable AUXILIUM_ARG_xxx, where xxx is the name of your argument
* At the end of your script, you have to call auxilium_close for delete temporaries files

## Shell Completion
* An option (--sh_completion) is automatically add wich allow to generate a bash or zsh completion file

## In progress
The support of sub commands is actually in progress